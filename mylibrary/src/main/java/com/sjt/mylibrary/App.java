package com.sjt.mylibrary;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheEntity;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.cookie.CookieJarImpl;
import com.lzy.okgo.cookie.store.DBCookieStore;
import com.lzy.okgo.interceptor.HttpLoggingInterceptor;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.sjt.mylibrary.bean.ActivityBean;
import com.sjt.mylibrary.dictionary.SpKey;
import com.sjt.mylibrary.utils.CommonSp;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import okhttp3.OkHttpClient;

public class App extends Application {
    public static String token;
    public static String uid;
    private static Context mContext;

    private static List<ActivityBean> activityList = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        initOkGo();
        initToken();
        initAop();
    }

    public void initToken() {
        token = CommonSp.getInstance(getContext()).getString(SpKey.SP_TOKEN, "");
        uid = CommonSp.getInstance(getContext()).getString(SpKey.SP_USERID, "");
    }

    public static void setToken(String token) {
        App.token = token;
        CommonSp.getInstance(getContext()).putString(SpKey.SP_TOKEN, token);
    }

    public static void setUid(String uid) {
        App.uid = uid;
        CommonSp.getInstance(getContext()).putString(SpKey.SP_USERID, uid);
    }

    public static void setLogin(boolean b) {
        CommonSp.getInstance(getContext()).putBoolean(SpKey.SP_LOGIN, b);
    }

    public static boolean getLogin() {
        return CommonSp.getInstance(getContext()).getBoolean(SpKey.SP_LOGIN, false);
    }

    public void initOkGo() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        //builder.cookieJar(new CookieJarImpl(new SPCookieStore(this)));      //使用sp保持cookie，如果cookie不过期，则一直有效
        builder.cookieJar(new CookieJarImpl(new DBCookieStore(this)));//使用数据库保持cookie，如果cookie不过期，则一直有效
        //builder.cookieJar(new CookieJarImpl(new MemoryCookieStore()));      //使用内存保持cookie，app退出后，cookie消失
        //设置请求头
        HttpHeaders headers = new HttpHeaders();
        headers.put("Content-Type", "application/x-www-form-urlencoded");    //header不支持中文，不允许有特殊字符
//        headers.put("commonHeaderKey2", "commonHeaderValue2");
        //设置请求参数
        HttpParams params = new HttpParams();
        params.put("application_type", "property");     //param支持中文,直接传,不要自己编码
        params.put("application_id", "1");

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor("OkGo");
        //log打印级别，决定了log显示的详细程度
        loggingInterceptor.setPrintLevel(HttpLoggingInterceptor.Level.BODY);
        //log颜色级别，决定了log在控制台显示的颜色
        loggingInterceptor.setColorLevel(Level.INFO);
        builder.addInterceptor(loggingInterceptor);
        OkGo.getInstance().init(this)                           //必须调用初始化
                .setOkHttpClient(builder.build())               //建议设置OkHttpClient，不设置会使用默认的
                .setCacheMode(CacheMode.NO_CACHE)               //全局统一缓存模式，默认不使用缓存，可以不传
                .setCacheTime(CacheEntity.CACHE_NEVER_EXPIRE)   //全局统一缓存时间，默认永不过期，可以不传
                .setRetryCount(3)
//                .addCommonHeaders(headers)//全局公共头
//                .addCommonParams(params)//全局公共参数
        ;
    }


    public void initAop() {
//        XAOP.init(this); //初始化插件
//        XAOP.debug(true); //日志打印切片开启
//        XAOP.setPriority(Log.INFO); //设置日志打印的等级,默认为0
//
//        //设置动态申请权限切片 申请权限被拒绝的事件响应监听
//        XAOP.setOnPermissionDeniedListener(new PermissionUtils.OnPermissionDeniedListener() {
//            @Override
//            public void onDenied(List<String> permissionsDenied) {
//                //申请权限被拒绝的处理
//            }
//
//        });
    }


    public static Context getContext() {
        return mContext;
    }

    public static void addActivity(Activity activity, String tag) {
        activityList.add(new ActivityBean(activity, tag));
    }

    public static void destoryActivity(String tag) {
        for (ActivityBean ac : activityList) {
            if (tag.equals(ac.getTag())) ac.getActivity().finish();
        }
    }

}
