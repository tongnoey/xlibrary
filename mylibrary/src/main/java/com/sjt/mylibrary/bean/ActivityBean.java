package com.sjt.mylibrary.bean;

import android.app.Activity;

public class ActivityBean {
    private Activity activity;
    private String tag;

    public ActivityBean(Activity activity, String tag) {
        this.activity = activity;
        this.tag = tag;
    }

    public ActivityBean(){

    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
