package com.sjt.mylibrary.dictionary;

public class SpKey {
    public static final String SP_PHONE = "sp_phone";
    public static final String SP_PASSW = "sp_passw";
    public static final String SP_USER = "sp_user";
    public static final String SP_TOKEN = "sp_token";
    public static final String SP_USERID = "sp_userid";
    public static final String SP_USERROLE = "sp_userrole";
    public static final String SP_USERNAME = "sp_username";
    public static final String SP_USERPHONE = "sp_userphone";
    public static final String SP_USERHEAD = "sp_userhead";
    public static final String SP_USER_AUTHED = "sp_user_authed";
    public static final String SP_LOGIN = "sp_login";
}
