package com.sjt.mylibrary.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.lang.reflect.Type;
import java.util.Map;

public class CommonSp {

    private String TAG = "property";
    private static SharedPreferences sp;
    private static CommonSp mInstance;

    private CommonSp(Context context) {
        sp = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE);
    }

    public static CommonSp getInstance(Context context) {
        if (mInstance == null) {
            synchronized (CommonSp.class) {
                if (mInstance == null) {
                    mInstance = new CommonSp(context);
                }
            }
        }
        return mInstance;
    }

    //=======================================键值保存==================================================//

    /**
     * 设置boolean值
     *
     * @param key
     * @param value
     */
    public    boolean putBoolean(String key, boolean value) {
        return sp.edit().putBoolean(key, value).commit();
    }

    /**
     * 设置float值
     *
     * @param key
     * @param value
     */
    public    boolean putFloat(String key, float value) {
        return sp.edit().putFloat(key, value).commit();
    }

    /**
     * 设置long值
     *
     * @param key
     * @param value
     */
    public    boolean putLong(String key, long value) {
        return sp.edit().putLong(key, value).commit();
    }

    /**
     * 设置String值
     *
     * @param key
     * @param value
     */
    public boolean putString(String key, String value) {
        return sp.edit().putString(key, value).commit();
    }

    /**
     * 设置int值
     *
     * @param key
     * @param value
     */
    public  boolean putInt(String key, int value) {
        return sp.edit().putInt(key, value).commit();
    }

    /**
     * 设置Object
     *
     * @param key
     * @param value
     * @return
     */
    public  boolean putObject(String key, Object value) {
        return sp.edit().putString(key, JsonUtil.toJson(value)).commit();
    }


    //=======================================键值获取==================================================//

    /**
     * 根据key获取boolean值
     *
     * @param key
     * @param defValue
     * @return
     */
    public  boolean getBoolean(String key, boolean defValue) {
        try {
            return sp.getBoolean(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defValue;
    }

    /**
     * 根据key获取long值
     *
     * @param key
     * @param defValue
     * @return
     */
    public    long getLong(String key, long defValue) {
        try {
            return sp.getLong(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defValue;
    }

    /**
     * 根据key获取float值
     *
     * @param key
     * @param defValue
     * @return
     */
    public    float getFloat(String key, float defValue) {
        try {
            return sp.getFloat(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defValue;
    }

    /**
     * 根据key获取String值
     *
     * @param key
     * @param defValue
     * @return
     */
    public    String getString(String key, String defValue) {
        try {
            return sp.getString(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defValue;
    }

    /**
     * 根据key获取int值
     *
     * @param key
     * @param defValue
     * @return
     */
    public    int getInt(String key, int defValue) {
        try {
            return sp.getInt(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defValue;
    }


    /**
     * 获取对象
     *
     * @param key
     * @param type 泛型类
     * @param <T>
     * @return
     */
    public    <T> T getObject(String key, Type type) {
        return JsonUtil.fromJson(getString(key, ""), type);
    }

    /**
     * 得到保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
     *
     * @param key
     * @param defaultObject
     * @return
     */
    public    Object get(String key, Object defaultObject) {
        try {
            if (defaultObject instanceof String) {
                return sp.getString(key, (String) defaultObject);
            } else if (defaultObject instanceof Integer) {
                return sp.getInt(key, (Integer) defaultObject);
            } else if (defaultObject instanceof Boolean) {
                return sp.getBoolean(key, (Boolean) defaultObject);
            } else if (defaultObject instanceof Float) {
                return sp.getFloat(key, (Float) defaultObject);
            } else if (defaultObject instanceof Long) {
                return sp.getLong(key, (Long) defaultObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defaultObject;
    }


    //=======================================公共方法==================================================//

    /**
     * 查询某个key是否已经存在
     *
     * @param key
     * @return
     */
    public    boolean contains(String key) {
        return sp.contains(key);
    }

    /**
     * 返回所有的键值对
     *
     * @return
     */
    public    Map<String, ?> getAll() {
        try {
            return sp.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 去除某一键值对
     *
     * @param key
     * @return
     */
    public    boolean remove(String key) {
        return sp.edit().remove(key).commit();
    }

    /**
     * 清空销毁
     *
     */
    public    boolean clear() {
        return sp.edit().clear().commit();
    }
}
