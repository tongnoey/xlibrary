package com.sjt.mylibrary.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.sjt.mylibrary.R;

public class GlideUtils {
    public static void display(Context context, String url, ImageView iv){
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.color.gray_border)
                .diskCacheStrategy(DiskCacheStrategy.NONE);
        Glide.with(context)
                .load(url)
                .apply(options)
                .into(iv);
    }
}
