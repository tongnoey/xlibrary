package com.sjt.mylibrary.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;


public class TimeUtil {
    /**
     * 毫秒
     */
    public final static int MILLISECOND = Calendar.MILLISECOND;

    /**
     * 秒
     */
    public final static int SECOND = Calendar.SECOND;

    /**
     * 分
     */
    public final static int MINUTE = Calendar.MINUTE;

    /**
     * 小时
     */
    public final static int HOUR = Calendar.HOUR;

    /**
     * 天
     */
    public final static int DAY = Calendar.DATE;

    /**
     * 月
     */
    public final static int MONTH = Calendar.MONTH;

    /**
     * 年
     */
    public final static int YEAR = Calendar.YEAR;

    /**
     * 默认的时间戳格式
     */
    public final static String DEFAULT_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * 默认的时间格式
     */
    public final static String DEFAULT_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 默认的日期格式
     */
    public final static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    /**
     * 将时间字符串转为时间戳
     * <p>time 格式为 format</p>
     *
     * @param time   时间字符串
     * @param format 时间格式
     * @return 毫秒时间戳
     */
    public static long string2Millis(final String time, final String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            if (sdf != null) {
                return sdf.parse(time).getTime();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 将时间转换为缺省时间格式字符串
     *
     * @param time 待转换的时间
     * @return 转换结果，如果转换失败则返回 null
     */
    public static String convTime(Date time) {
        return convTime(time, DEFAULT_TIME_FORMAT);
    }

    /**
     * 将时间转换为指定格式字符串
     *
     * @param time   待转换的时间
     * @param format 格式字符串，具体格式请参见JavaDoc中java.text.SimpleDateFormat
     * @return 转换结果
     */
    public static String convTime(Date time, String format) {
        if (time == null) {
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(time);
    }

    /**
     * 将一个时间字符串转换为缺省时间格式
     *
     * @param time 待转换的时间
     * @return 转换结果，如果转换失败则返回 null
     */
    public static String convTime(String time) {
        return convTime(time, DEFAULT_TIME_FORMAT);
    }

    /**
     * 将时间字符串转换为指定格式
     *
     * @param time   待转换的时间
     * @param format 格式字符串，具体格式请参见JavaDoc中java.text.SimpleDateFormat
     * @return 转换结果
     */
    public static String convTime(String time, String format) {
        if (time == null) {
            return null;
        }

        Date d = parseTime(time);
        return convTime(d, format);
    }

    /**
     * 将long型的时间转换为缺省格式时间字符串
     *
     * @param time 待转换的时间
     * @return 转换结果
     */
    public static String convTime(long time) {
        return convTime(time, DEFAULT_TIME_FORMAT);
    }

    /**
     * 将long型的时间转换为指定格式时间字符串
     *
     * @param time   待转换的时间
     * @param format 格式字符串，具体格式请参见JavaDoc中java.text.SimpleDateFormat
     * @return 转换结果
     */
    public static String convTime(long time, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(time);
    }

    /**
     * 将时间字符串解析为Date型
     *
     * @param time 待解析的字符串
     * @return 解析结果，如果解析失败则返回 null
     */
    public static Date parseTime(String time) {
        return parseTime(time, null, null, null);

    }

    /**
     * 将时间字符串按照指定格式解析为Date型
     *
     * @param time   待解析的时间字符串
     * @param format 格式字符串，具体格式请参见JavaDoc中java.text.SimpleDateFormat
     * @return 解析结果，如果解析失败则返回 null
     */
    public static Date parseTime(String time, String format) {
        return parseTime(time, format, null, null);
    }

    /**
     * 将时间字符串按照指定格式解析为Date型
     *
     * @param time     待解析的时间字符串
     * @param timeZone 时区
     * @return 解析结果，如果解析失败则返回 null
     */
    public static Date parseTime(String time, TimeZone timeZone) {
        return parseTime(time, null, timeZone);
    }

    /**
     * 将时间字符串按照指定格式解析为Date型
     *
     * @param time     待解析的时间字符串
     * @param format   格式字符串，具体格式请参见JavaDoc中java.text.SimpleDateFormat
     * @param timeZone 时区
     * @return 解析结果，如果解析失败则返回 null
     */
    public static Date parseTime(String time, String format, TimeZone timeZone) {
        return parseTime(time, format, timeZone, null);
    }

    /**
     * 将时间字符串按照指定格式解析为Date型
     *
     * @param time   待解析的时间字符串
     * @param format 格式字符串，具体格式请参见JavaDoc中java.text.SimpleDateFormat
     * @param locale 语言环境
     * @return 解析结果，如果解析失败则返回 null
     */
    public static Date parseTime(String time, String format, Locale locale) {
        return parseTime(time, format, null, locale);
    }


    /**
     * 将时间字符串按照指定格式解析为Date型
     *
     * @param time     待解析的时间字符串
     * @param format   格式字符串，具体格式请参见JavaDoc中java.text.SimpleDateFormat
     * @param timeZone 时区
     * @param locale   语言环境
     * @return 解析结果，如果解析失败则返回 null
     */
    public static Date parseTime(String time, String format, TimeZone timeZone,
                                 Locale locale) {
        if (time == null) {
            return null;
        }

        String[] astrPatterns = null;

        if (format != null) {
            astrPatterns = new String[]{format};
        } else {
            astrPatterns = new String[]{"yy/MM/dd HH:mm:ss.SSS",
                    "yyyy-MM-dd HH:mm:ss.SSS", "yyyy/MM/dd HH:mm:ss.SSS",
                    "yyyyMMdd HH:mm:ss.SSS", "yyyy-MM-dd HH:mm:ss",
                    "yyyy/MM/dd HH:mm:ss", "yyyyMMddhhmmss", "yy/MM/dd",
                    "yyyy-MM-dd", "yyyy/MM/dd", "yyyyMMdd"};
        }

        Date dateResult = null;

        if (locale == null) {
            locale = Locale.getDefault();
        }

        for (int i = 0; i < astrPatterns.length; i++) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(astrPatterns[i],
                        locale);

                if (timeZone != null) {
                    sdf.setTimeZone(timeZone);
                }

                dateResult = sdf.parse(time);
            } catch (Exception e) {
            }

            if (dateResult != null) {
                break;
            }
        }

        if (dateResult == null) {
            Log.w("[UTIL]", "日期字符串解析失败[" + time + "]");
        }

        return dateResult;
    }

    /**
     * 将两个时间字符串相减
     *
     * @param d1 被减数
     * @param d2 减数
     * @return 两个时间相减的毫秒数，如果有一个时间有误则返回0
     */
    public static long decTime(Date d1, Date d2) {
        if (d1 == null) {
            return 0;
        }

        if (d2 == null) {
            return 0;
        }

        long lDiff = d1.getTime() - d2.getTime();

        return lDiff;
    }

    public static String decTimeToMS(Date d1, Date d2) {
        long duration = decTime(d1, d2);
        return timeParse(duration);
    }

    /**
     * 计算在 x分钟内还剩余 分秒
     *
     * @param d1
     * @param d2
     * @param min
     * @return
     */
    public static String decTimeToXmLeft(Date d1, Date d2, int min) {
        long t = min * 60000;
        long duration = decTime(d1, d2);
        if (duration >= t) return "已超时";
        return timeParse(t - duration);
    }

    public static long decTimeToxmLongLeft(Date d1, Date d2, int min) {
        long t = min * 60000;
        long duration = decTime(d1, d2);
        if (duration >= t) return 0;
        return t - duration;
    }

    /**
     * 将时间差换算成分秒
     *
     * @param duration
     * @return
     */
    public static String timeParse(long duration) {
        StringBuffer time = new StringBuffer();
        long minute = duration / 60000;
        long seconds = duration % 60000;
        long second = Math.round((float) seconds / 1000);
        time.append(minute).append("分").append(second).append("秒");
        return time.toString();
    }


    public static final String getdate() {
        return convTime(System.currentTimeMillis(), "y-M-d");
    }

    public static final String getTime() {
        return convTime(System.currentTimeMillis(), "HH:mm");
    }

    public static Date rollTime(Date time, int field, int delta) {
        GregorianCalendar calendar = new GregorianCalendar();

        calendar.setTime(time);
        calendar.add(field, delta);
        return calendar.getTime();
    }

    /**
     * 获取系统时间（精确到秒）
     *
     * @return
     */
    public static final String getDateTime() {
        return convTime(System.currentTimeMillis(), DEFAULT_TIME_FORMAT);
    }

    public static final String getDateTimess() {
        return convTime(System.currentTimeMillis(), "yyyy-MM-dd 00:00:00");
    }


    /**
     * @param time 要判断是否在当天24h内的时间
     * @return boolean
     * @Description 是否为当天24h内
     * @author 刘鹏博
     */
    public static boolean isToday(String time) {


        //测试时间1
        String testTimeOne = time;


        //时间格式化
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date psTestTimeOne = null;

        try {
            psTestTimeOne = format1.parse(testTimeOne);

        } catch (Exception e) {
            e.printStackTrace();
        }


        boolean flag = false;
        //获取当前系统时间
        long longDate = System.currentTimeMillis();
        Date nowDate = new Date(longDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = dateFormat.format(nowDate);
        String subDate = format.substring(0, 10);
        //定义每天的24h时间范围
        String beginTime = subDate + " 00:00:00";
        String endTime = subDate + " 23:59:59";
        Date paseBeginTime = null;
        Date paseEndTime = null;
        try {
            paseBeginTime = dateFormat.parse(beginTime);
            paseEndTime = dateFormat.parse(endTime);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (psTestTimeOne.after(paseBeginTime) && psTestTimeOne.before(paseEndTime)) {
            flag = true;
        }
        return flag;
    }

    /**
     * @return 返回值描述
     * @throws <异常类型> {@inheritDoc} 异常描述
     * @author 作者:孙磊 方法描述:获取某天，-1是昨天，0是今天，1是明天依次类推
     * @param参数描述 格式M月dd日这样的话显示的就是3月12日，否则 MM月dd日格式会选择03月12日
     */

    public static String getmoutianMD(int i) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        String dateString = "";
        try {
            calendar.add(calendar.DATE, i);//把日期往后增加一天.整数往后推,负数往前移动
            date = calendar.getTime(); //这个时间就是日期往后推一天的结果
            dateString = formatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }


    /**
     * 方法描述:获取某天，-1是昨天，0是今天，1是明天依次类推
     *
     * @param i
     * @return
     */
    public static String getmoutianY_S(int i) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(DEFAULT_TIME_FORMAT);
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        String dateString = "";
        try {
            calendar.add(calendar.DATE, i);//把日期往后增加一天.整数往后推,负数往前移动
            date = calendar.getTime(); //这个时间就是日期往后推一天的结果
            dateString = formatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }


    private static long getWeeOfToday() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    public static int isTimeInOrOut(String start,String end){
        long current = System.currentTimeMillis();
        if(current < string2Millis(start,DEFAULT_TIME_FORMAT))return -1;
        if(current < string2Millis(end,DEFAULT_TIME_FORMAT))return 0;
        else return 1;
    }

}
