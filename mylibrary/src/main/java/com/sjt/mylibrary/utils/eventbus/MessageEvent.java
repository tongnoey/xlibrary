package com.sjt.mylibrary.utils.eventbus;

public class MessageEvent {

    private String message;
    private int code;
    private int value;

    public MessageEvent(int code, int value) {
        this.code = code;
        this.value = value;
    }
    public MessageEvent(int code, String message) {
        this.code = code;
        this.message = message;
    }
    public MessageEvent() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
