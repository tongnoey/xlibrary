package com.sjt.mylibrary.utils.net;

public class ComRespons<T> {
    /**
     * "response": "000000",
     * "message": "操作成功",
     **/

    public String response;
    public String message;

    public T data;

}
