package com.sjt.mylibrary.utils.net;

public class SimpleResponse {
    public String response;
    public String message;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
