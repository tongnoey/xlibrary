package com.sjt.mylibrary.wdigit.broccoli;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.samlss.broccoli.Broccoli;

/**
 * 使用Broccoli占位的基础适配器
 *
 * @author XUE
 * @since 2019/4/8 16:33
 */
public abstract class BroccoliRecyclerAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder> {
    /**
     * 是否已经加载成功
     */
    private boolean mHasLoad = false;
    private Map<View, Broccoli> mBroccoliMap = new HashMap<>();

    public BroccoliRecyclerAdapter(int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder holder, T t) {
        Broccoli broccoli = mBroccoliMap.get(holder.itemView);
        if (broccoli == null) {
            broccoli = new Broccoli();
            mBroccoliMap.put(holder.itemView, broccoli);
        }
        if (mHasLoad) {
            broccoli.removeAllPlaceholders();

            onBindData(holder, t);
        } else {
            onBindBroccoli(holder, broccoli);
            broccoli.show();
        }
    }


    /**
     * 绑定控件
     *
     * @param holder
     * @param model
     */
    protected abstract void onBindData(BaseViewHolder holder, T model);

    /**
     * 绑定占位控件
     *
     * @param broccoli
     */
    protected abstract void onBindBroccoli(BaseViewHolder holder, Broccoli broccoli);


    @Override
    public void setNewInstance(@Nullable List<T> list) {
        mHasLoad = true;
        super.setNewInstance(list);
    }

    /**
     * 资源释放，防止内存泄漏
     */
    public void recycle() {
        for (Broccoli broccoli : mBroccoliMap.values()) {
            broccoli.removeAllPlaceholders();
        }
        mBroccoliMap.clear();
        setNewInstance(new ArrayList<>());
    }

}
