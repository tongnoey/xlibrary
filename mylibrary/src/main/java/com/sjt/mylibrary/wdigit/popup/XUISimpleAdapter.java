package com.sjt.mylibrary.wdigit.popup;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ruffian.library.widget.RTextView;
import com.sjt.mylibrary.R;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public class XUISimpleAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> datas;

    public XUISimpleAdapter(Context context, @Nullable List<String> data) {
        mContext = context;
        datas = data;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public String getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.pop_simple_item, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.tv.setText(getItem(position));
        holder.tv.setSelected(true);
        return convertView;
    }

    class Holder {
        RTextView tv;

        Holder(View view) {
            tv = view.findViewById(R.id.tv);
        }
    }
}
