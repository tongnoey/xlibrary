package com.sjt.mylibrary.wdigit.xbanner;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.sjt.mylibrary.R;
import com.sjt.mylibrary.wdigit.xbanner.base.BaseIndicatorBanner;


public class GuideBanner extends BaseIndicatorBanner<Object, GuideBanner> {
    public GuideBanner(Context context) {
        super(context);
        onCreateBanner();
    }

    public GuideBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
        onCreateBanner();
    }

    public GuideBanner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        onCreateBanner();
    }

    protected void onCreateBanner() {
        setBarShowWhenLast(true);
        //不进行自动滚动
        setAutoScrollEnable(false);
    }


    @Override
    public View onCreateItemView(int position) {
        View inflate = View.inflate(mContext, R.layout.xbanner_simple_image, null);
//        iv = inflate.findViewById(R.id.iv);
//        ImageView text = inflate.findViewById(R.id.text);
//        GuideImgBean bean = (GuideImgBean)mDatas.get(position);
//        iv.setGifResource(bean.getIvResId());
//        iv.play();
//        iv.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                iv.pause();
//            }
//        },5000);
//        text.setImageResource(bean.getTextResId());
//        return inflate;
        return   inflate;
    }

   /* @Override
    public void setCurrentIndicator(int position) {
        super.setCurrentIndicator(position);
        if(iv==null)return;
        iv.play();
        iv.postDelayed(new Runnable() {
            @Override
            public void run() {
                iv.pause();
            }
        },1000);
    }*/
}
