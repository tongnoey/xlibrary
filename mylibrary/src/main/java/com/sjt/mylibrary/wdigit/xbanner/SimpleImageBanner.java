package com.sjt.mylibrary.wdigit.xbanner;

import android.content.Context;
import android.util.AttributeSet;

import com.sjt.mylibrary.R;
import com.sjt.mylibrary.wdigit.xbanner.base.BaseImageBanner;


public class SimpleImageBanner extends BaseImageBanner<SimpleImageBanner> {

    public SimpleImageBanner(Context context) {
        super(context);
    }

    public SimpleImageBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SimpleImageBanner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected int getItemLayoutId() {
        return R.layout.xbanner_simple_image;
    }

    @Override
    protected int getImageViewId() {
        return R.id.iv;
    }

}
