package com.sjt.mylibrary.wdigit.xbanner.base;

/**
 * 图片轮播条目
 *
 * @author xuexiang
 * @since 2018/11/25 下午7:01
 */
public class BannerItem {
    public String title;


    public String img;


    public String link;

    public String getImgUrl() {
        return img;
    }

    public BannerItem setImgUrl(String imgUrl) {
        this.img = imgUrl;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public BannerItem setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getLink() {
        return link;
    }

    public BannerItem setLink(String link) {
        this.link = link;
        return this;
    }
}
